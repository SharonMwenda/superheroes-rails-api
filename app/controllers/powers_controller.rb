class PowersController < ApplicationController
rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity

    def index
        powers = Power.all
        render json: powers
    end

    def show
        power = Power.find_by(id: params[:id])
         render json: power
    end

    def update
        power = Power.find_by(id: params[:id])
        power.update!(power_params)
        render json: power, status: :accepted
    end

    private
    def power_params
        params.permit(:description)
    end

    def render_not_found
        render json: {error: 'power not found'}, status: :not_found
    end

    def render_unprocessable_entity(invalid)
        render json: {errors: invalid.record.errors}, status: :unprocessable_entity
    end
end
